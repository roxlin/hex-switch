﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HexSwitch
{
    class Program
    {
        public static string Encode32 = "123456789ABCDEFGHJKMNPQRSTUVWXYZ";
        public static string Encode34 = "0123456789ABCDEFGHJKLMNPQRSTUVWXYZ";
        public static string Encode36 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        static void Main(string[] args)
        {
            int i = 0; int hex = 1;
            string data = "76543210";
            Console.WriteLine("程序每次转换1000条，输入进制0退出程序。");
            Console.Write("请输入要转换的进制：");
            hex = Convert.ToInt32(Console.ReadLine());
            while (hex != 0)
            {
                Console.Write("请输入要转换的数字：");
                data = Console.ReadLine();
                while (i++ < 1000)
                {
                    data = StringAdd(data, hex);
                }
                i = 0;
                Console.Write("请输入要转换的进制：");
                hex = Convert.ToInt32(Console.ReadLine());
            }
        }

        public static string StringToInt36(string input)
        {
            input = input.ToUpper();
            StringBuilder result = new StringBuilder(input.Length * 2);
            foreach (var item in input.ToCharArray())
            {
                int asci = item;
                result.Append(asci);
            }
            Console.WriteLine("StringToInt36:" + result.ToString());
            return result.ToString();
        }

        public static string StringToInt34(string input)
        {
            input = input.ToUpper();
            StringBuilder result = new StringBuilder(input.Length * 2);
            foreach (var item in input.ToCharArray())
            {
                int asci = item;
                if (Encode34.IndexOf(item) < 0) asci += 1;
                result.Append(asci);
            }
            Console.WriteLine("StringToInt34:" + result.ToString());
            return result.ToString();
        }

        public static string StringToInt32(string input)
        {
            input = input.ToUpper();
            StringBuilder result = new StringBuilder(input.Length * 2);
            foreach (var item in input.ToCharArray())
            {
                int asci = item;
                if (Encode32.IndexOf(item) < 0) asci += 1;
                result.Append(asci);
            }
            Console.WriteLine("StringToInt32:" + result.ToString());
            return result.ToString();
        }

        public static string StringFromInt(string input)
        {
            int numOfBytes = input.Length / 2;
            StringBuilder result = new StringBuilder(numOfBytes);
            for (int i = 0; i < numOfBytes; ++i)
            {
                result.Append((char)int.Parse(input.Substring(2 * i, 2)));
            }
            Console.WriteLine("StringFromInt:" + result.ToString());
            return result.ToString();
        }

        public static string StringAdd(string input, int provider, int op = 1)
        {
            input = input.ToUpper();
            StringBuilder data = new StringBuilder(input.Length * 2);
            var arry = input.ToCharArray();
            int param = provider == 32 || provider == 34 ? 36 : provider;
            for (int i = arry.Length - 1; i >= 0; i--)
            {
                int asci = arry[i];
                //字母和数字ASSII码并
                asci = asci > 64 ? asci - 7 : asci;
                //进位
                if (op > 0)
                {
                    int remainder = op % param;
                    op /= param;
                    asci += remainder - 47;
                    if ((asci / param > 0 && asci % param > 0) || asci / param > 1)
                    {
                        op++;
                    }
                    asci = asci % param == 0 ? param : asci % param;
                    asci += 47;
                }
                if (asci > 57)
                {
                    asci += 7;
                }
                if (Encode32.IndexOf((char)asci) < 0 && provider == 32) asci += 1;
                if (Encode34.IndexOf((char)asci) < 0 && provider == 34) asci += 1;
                data.Insert(0, asci);
            }
            var result = StringFromInt(data.ToString());
            Console.WriteLine($"StringAdd{provider}: {input} to " + result);
            return result;
        }
    }
}
